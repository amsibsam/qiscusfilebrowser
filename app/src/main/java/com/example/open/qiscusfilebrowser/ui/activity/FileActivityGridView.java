package com.example.open.qiscusfilebrowser.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.open.qiscusfilebrowser.R;
import com.example.open.qiscusfilebrowser.infrastructure.client.QiscusClient;
import com.example.open.qiscusfilebrowser.model.QiscusFile;
import com.example.open.qiscusfilebrowser.model.event.ClientCallback;
import com.example.open.qiscusfilebrowser.ui.adapter.FileAdapter;
import com.example.open.qiscusfilebrowser.ui.adapter.FileAdapterGridView;

import java.util.List;

public class FileActivityGridView extends AppCompatActivity implements ClientCallback<QiscusFile> {
    private Toolbar toolbar;
    private GridView GridViewFile;
    private FileAdapterGridView adapter;
    private List<QiscusFile> files;
    private Dialog settingsDialog;
    private TextView urlFile;
    private ImageButton close;
    private String namaTopic;
    private int idtopic;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_gridview);

        Window w = getWindow();
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        GridViewFile = (GridView) findViewById(R.id.GridViewFile);
        close = (ImageButton) findViewById(R.id.imageButtonClose);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        settingsDialog = new Dialog(this);


        Bundle extras = getIntent().getExtras();
        idtopic = extras.getInt("idtopic");
        namaTopic = extras.getString("namaTopic");
        setTitle(namaTopic);
        System.out.println("idtopic di fileactivity "+idtopic);
        files = QiscusClient.getFileTopic(idtopic, QiscusClient.token, FileActivityGridView.this);

        adapter = new FileAdapterGridView(this, R.layout.item_file_grid, files);
        GridViewFile.setAdapter(adapter);
        System.out.println("set adapter");

        GridViewFile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("ini click posisi "+position);
            }
        });



    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_file_grid, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onSucceeded(QiscusFile result) {
        adapter.notifyDataSetChanged();
        System.out.println("notify di file");
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void onNoConnection() {

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.GridView :
                System.out.println("asd");
                Intent intent = new Intent(this, FileActivity.class);
                intent.putExtra("idtopic", idtopic);
                intent.putExtra("namaTopic", namaTopic);
                finish();
                startActivity(intent);break;
            }
        return super.onOptionsItemSelected(item);
    }
}
